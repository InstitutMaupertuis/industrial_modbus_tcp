[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Overview
This package contains a ROS node and a RViz panel to allow users to fetch data over Modbus TCP and publish them on ROS topics.

It can be used through the service (command line or programming) and also thanks to a RViz panel.

- Multiple handlers can be created to establish as many connections as needed to Modbus TCP servers
- Multiple data can be fetched at the same time
- For each data the rate, endiannes and topic can be specified

Supported types are:
- bool
- int8
- uint8
- int16
- uint16
- int32
- uint32
- int64
- uint64
- float32
- float64

Depending on the data width multiple registers are fetched. Eg: `int32` requires to fetch two modbus registers (16 bits each)

:information_source: The panel configuration is saved/loaded in the RViz configuration file, this includes the base topic and handlers configuration. You don't need to configure the handlers again.

:warning: When trying to connect to an IP address that is not reachable / does not host a Modbus TCP server the service will hang for a very long time (~ 2 minutes)

# Dependencies
## rosdep
Install, initialize and update [rosdep](https://wiki.ros.org/rosdep).

# Compiling
Create a catkin workspace and clone the project:

```bash
mkdir -p catkin_workspace/src
cd catkin_workspace/src
git clone https://gitlab.com/InstitutMaupertuis/industrial_modbus_tcp.git
cd ..
```

## Resolve ROS dependencies
```bash
rosdep install --from-paths src --ignore-src --rosdistro $ROS_DISTRO -y
```

## Compile
```bash
catkin_make
```

# User manual
```bash
rosrun industrial_modbus_tcp industrial_modbus_tcp &
rviz
```

`Panels` > `Add New Panel` > `industrial_modbus_tcp/ModbusTCP`

![Base topic selection](documentation/panel_base_topic.png)

![Panel](documentation/panel.png)

![Panel configure](documentation/panel_configure.png)
