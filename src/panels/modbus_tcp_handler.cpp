#include <industrial_modbus_tcp/panels/modbus_tcp_handler.hpp>

namespace industrial_modbus_tcp
{

ModbusTCPHandler::ModbusTCPHandler(industrial_modbus_tcp::ModbusHandler h)
{
  layout_ = new QVBoxLayout();
  setLayout(layout_);

  name_ = new QLineEdit(QString::fromStdString(h.name));
  connect(name_, &QLineEdit::textChanged, this, &ModbusTCPHandler::nameChanged);
  ip_address_ = new QLineEdit(QString::fromStdString(h.ip_address));
  ip_address_->setInputMask("000.000.000.000;_");
  port_ = new QSpinBox;
  port_->setRange(0, 1000);
  port_->setValue(h.port);
  recovery_ = new QComboBox;
  recovery_->addItem("None");
  recovery_->addItem("Link");
  recovery_->addItem("Protocol");
  recovery_->addItem("Link and protocol");
  recovery_->setCurrentIndex(h.modbus_error_recovery_mode);

  tabs_ = new QTabWidget;
  for (std::size_t i(0); i < TAB_INDEX::SIZE; ++i)
    tables_.emplace_back(new QTableWidget);

  {
    QStringList labels;
    labels.push_back("Topic name");
    labels.push_back("Register address");
    labels.push_back("Frequency (Hz)");
    labels.push_back("Mask");
    tables_.at(BOOL)->setColumnCount(labels.size());
    tables_.at(BOOL)->setHorizontalHeaderLabels(labels);
  }
  {
    QStringList labels;
    labels.push_back("Topic name");
    labels.push_back("Register address");
    labels.push_back("Frequency (Hz)");
    labels.push_back("Bit shift");
    tables_.at(INT8)->setColumnCount(labels.size());
    tables_.at(INT8)->setHorizontalHeaderLabels(labels);
    tables_.at(UINT8)->setColumnCount(labels.size());
    tables_.at(UINT8)->setHorizontalHeaderLabels(labels);
  }
  {
    QStringList labels;
    labels.push_back("Topic name");
    labels.push_back("Register address");
    labels.push_back("Frequency (Hz)");
    tables_.at(INT16)->setColumnCount(labels.size());
    tables_.at(INT16)->setHorizontalHeaderLabels(labels);
    tables_.at(UINT16)->setColumnCount(labels.size());
    tables_.at(UINT16)->setHorizontalHeaderLabels(labels);
  }
  {
    QStringList labels;
    labels.push_back("Topic name");
    labels.push_back("Register address");
    labels.push_back("Frequency (Hz)");
    labels.push_back("Endianness");
    tables_.at(INT32)->setColumnCount(labels.size());
    tables_.at(INT32)->setHorizontalHeaderLabels(labels);
    tables_.at(UINT32)->setColumnCount(labels.size());
    tables_.at(UINT32)->setHorizontalHeaderLabels(labels);
    tables_.at(INT64)->setColumnCount(labels.size());
    tables_.at(INT64)->setHorizontalHeaderLabels(labels);
    tables_.at(UINT64)->setColumnCount(labels.size());
    tables_.at(UINT64)->setHorizontalHeaderLabels(labels);
    tables_.at(FLOAT32)->setColumnCount(labels.size());
    tables_.at(FLOAT32)->setHorizontalHeaderLabels(labels);
    tables_.at(FLOAT64)->setColumnCount(labels.size());
    tables_.at(FLOAT64)->setHorizontalHeaderLabels(labels);
  }

  tabs_->addTab(tables_.at(BOOL), "bool");
  tabs_->addTab(tables_.at(INT8), "int8");
  tabs_->addTab(tables_.at(UINT8), "uint8");
  tabs_->addTab(tables_.at(INT16), "int16");
  tabs_->addTab(tables_.at(UINT16), "uint16");
  tabs_->addTab(tables_.at(INT32), "int32");
  tabs_->addTab(tables_.at(UINT32), "uint32");
  tabs_->addTab(tables_.at(INT64), "int64");
  tabs_->addTab(tables_.at(UINT64), "uint64");
  tabs_->addTab(tables_.at(FLOAT32), "float32");
  tabs_->addTab(tables_.at(FLOAT64), "float64");

  for (auto table : tables_)
  {
    table->setColumnWidth(0, 300);
    table->setColumnWidth(1, 120);
    table->setColumnWidth(2, 120);
  }

  add_data_ = new QPushButton("+");
  remove_data_ = new QPushButton("-");

  QHBoxLayout *name_layout = new QHBoxLayout;
  name_layout->addWidget(new QLabel("Name"));
  name_layout->addWidget(name_);

  QHBoxLayout *ip_layout = new QHBoxLayout;
  ip_layout->addWidget(new QLabel("IP address"));
  ip_layout->addWidget(ip_address_);

  QHBoxLayout *port_layout = new QHBoxLayout;
  port_layout->addWidget(new QLabel("Port"));
  port_layout->addWidget(port_);

  QHBoxLayout *recovery_layout = new QHBoxLayout;
  recovery_layout->addWidget(new QLabel("Recovery"));
  recovery_layout->addWidget(recovery_);

  QHBoxLayout *registers_op_layout = new QHBoxLayout;
  registers_op_layout->addWidget(add_data_);
  registers_op_layout->addWidget(remove_data_);
  connect(add_data_, &QPushButton::clicked, this, &ModbusTCPHandler::addData);
  connect(remove_data_, &QPushButton::clicked, this, &ModbusTCPHandler::removeData);

  layout_->addLayout(name_layout);
  layout_->addLayout(ip_layout);
  layout_->addLayout(port_layout);
  layout_->addLayout(recovery_layout);
  layout_->addWidget(tabs_);
  layout_->addLayout(registers_op_layout);

  for (auto d : h.d_bool)
    addBool(d);
  for (auto d : h.d_int8)
    addInt8(d);
  for (auto d : h.d_uint8)
    addUInt8(d);
  for (auto d : h.d_int16)
    addInt16(d);
  for (auto d : h.d_uint16)
    addUInt16(d);
  for (auto d : h.d_int32)
    addInt32(d);
  for (auto d : h.d_uint32)
    addUInt32(d);
  for (auto d : h.d_int64)
    addInt64(d);
  for (auto d : h.d_uint64)
    addUInt64(d);
  for (auto d : h.d_float32)
    addFloat32(d);
  for (auto d : h.d_float64)
    addFloat64(d);
}

ModbusTCPHandler::~ModbusTCPHandler()
{
}

void ModbusTCPHandler::readRegisterTable(
  const QTableWidget *table,
  const unsigned row,
  industrial_modbus_tcp::Register &r)
{
  r.topic_name = static_cast<QLineEdit *>(table->cellWidget(row, 0))->text().toStdString();
  r.address = static_cast<QSpinBox *>(table->cellWidget(row, 1))->value();
  r.poll_rate_usec = (double) 1e6 / static_cast<QDoubleSpinBox *>(table->cellWidget(row, 2))->value();
}

industrial_modbus_tcp::ModbusHandler ModbusTCPHandler::getHandler()
{
  industrial_modbus_tcp::ModbusHandler handler;
  handler.ip_address = ip_address_->text().toStdString();
  handler.name = name_->text().toStdString();
  handler.port = port_->value();
  handler.modbus_error_recovery_mode = recovery_->currentIndex();

  if (handler.name.empty())
    throw std::runtime_error("Handler name cannot be empty");

  {
    QTableWidget *table(tables_.at(BOOL));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::Bool d;
      readRegisterTable(table, i, d.reg);
      d.mask = static_cast<QSpinBox *>(table->cellWidget(i, 3))->value();
      handler.d_bool.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(INT8));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::Int8 d;
      readRegisterTable(table, i, d.reg);
      d.bit_shift = static_cast<QSpinBox *>(table->cellWidget(i, 3))->value();
      handler.d_int8.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(UINT8));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::UInt8 d;
      readRegisterTable(table, i, d.reg);
      d.bit_shift = static_cast<QSpinBox *>(table->cellWidget(i, 3))->value();
      handler.d_uint8.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(INT16));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::Int16 d;
      readRegisterTable(table, i, d.reg);
      handler.d_int16.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(UINT16));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::UInt16 d;
      readRegisterTable(table, i, d.reg);
      handler.d_uint16.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(INT32));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::Int32 d;
      readRegisterTable(table, i, d.reg);
      d.big_endian = static_cast<QComboBox *>(table->cellWidget(i, 3))->currentIndex();
      handler.d_int32.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(UINT32));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::UInt32 d;
      readRegisterTable(table, i, d.reg);
      d.big_endian = static_cast<QComboBox *>(table->cellWidget(i, 3))->currentIndex();
      handler.d_uint32.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(INT64));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::Int64 d;
      readRegisterTable(table, i, d.reg);
      d.big_endian = static_cast<QComboBox *>(table->cellWidget(i, 3))->currentIndex();
      handler.d_int64.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(UINT64));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::UInt64 d;
      readRegisterTable(table, i, d.reg);
      d.big_endian = static_cast<QComboBox *>(table->cellWidget(i, 3))->currentIndex();
      handler.d_uint64.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(FLOAT32));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::Float32 d;
      readRegisterTable(table, i, d.reg);
      d.big_endian = static_cast<QComboBox *>(table->cellWidget(i, 3))->currentIndex();
      handler.d_float32.emplace_back(d);
    }
  }

  {
    QTableWidget *table(tables_.at(FLOAT64));
    for (std::size_t i(0); i < (std::size_t) table->rowCount(); ++i)
    {
      industrial_modbus_tcp::Float64 d;
      readRegisterTable(table, i, d.reg);
      d.big_endian = static_cast<QComboBox *>(table->cellWidget(i, 3))->currentIndex();
      handler.d_float64.emplace_back(d);
    }
  }

  for (auto d : handler.d_bool)
    if (d.reg.topic_name.empty())
        throw std::runtime_error("Bool: Topic name cannot be empty!");

  for (auto d : handler.d_int8)
    if (d.reg.topic_name.empty())
        throw std::runtime_error("int8: Topic name cannot be empty!");

  for (auto d : handler.d_uint8)
    if (d.reg.topic_name.empty())
        throw std::runtime_error("uint8: Topic name cannot be empty!");

  for (auto d : handler.d_int16)
    if (d.reg.topic_name.empty())
        throw std::runtime_error("int16: Topic name cannot be empty!");

  for (auto d : handler.d_uint16)
    if (d.reg.topic_name.empty())
        throw std::runtime_error("uint16: Topic name cannot be empty!");

  for (auto d : handler.d_int32)
    if (d.reg.topic_name.empty())
        throw std::runtime_error("int32: Topic name cannot be empty!");

  for (auto d : handler.d_int64)
    if (d.reg.topic_name.empty())
        throw std::runtime_error("int64: Topic name cannot be empty!");

  for (auto d : handler.d_float32)
    if (d.reg.topic_name.empty())
        throw std::runtime_error("float32: Topic name cannot be empty!");

  for (auto d : handler.d_float64)
    if (d.reg.topic_name.empty())
        throw std::runtime_error("float64: Topic name cannot be empty!");

  return handler;
}

QString ModbusTCPHandler::name()
{
  return name_->text();
}

void ModbusTCPHandler::addRegister(const industrial_modbus_tcp::Register &r,
                                   QTableWidget *table)
{
  QLineEdit *topic_name = new QLineEdit(QString::fromStdString(r.topic_name));
  // Only letters, numbers and underscore
  topic_name->setValidator(new QRegExpValidator(QRegExp("[A-Za-z0-9_/]+"), this));
  table->setCellWidget(table->rowCount() - 1, 0, topic_name);

  QSpinBox *address = new QSpinBox;
  address->setRange(0, 0x20000);
  address->setValue(r.address);
  table->setCellWidget(table->rowCount() - 1, 1, address);

  QDoubleSpinBox *frequency = new QDoubleSpinBox;
  frequency->setRange(0.01, 1000);
  frequency->setSingleStep(1);
  frequency->setValue((double) 1e6 / r.poll_rate_usec);
  table->setCellWidget(table->rowCount() - 1, 2, frequency);
}

void ModbusTCPHandler::addBigEndian(const bool value, QTableWidget *table)
{
  QComboBox *truefalse(new QComboBox);
  truefalse->addItem("False");
  truefalse->addItem("True");
  truefalse->setCurrentIndex(value);
  table->setCellWidget(table->rowCount() - 1, 3, truefalse);
}

void ModbusTCPHandler::addBool(const industrial_modbus_tcp::Bool &d)
{
  QTableWidget *table(tables_.at(BOOL));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);

  QSpinBox *mask(new QSpinBox);
  mask->setRange(0, 65535);
  mask->setValue(d.mask);
  table->setCellWidget(table->rowCount() - 1, 3, mask);
}

void ModbusTCPHandler::addInt8(const industrial_modbus_tcp::Int8 &d)
{
  QTableWidget *table(tables_.at(INT8));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);

  QSpinBox *bit_shift(new QSpinBox);
  bit_shift->setRange(0, 16);
  bit_shift->setValue(d.bit_shift);
  table->setCellWidget(table->rowCount() - 1, 3, bit_shift);
}

void ModbusTCPHandler::addUInt8(const industrial_modbus_tcp::UInt8 &d)
{
  QTableWidget *table(tables_.at(UINT8));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);

  QSpinBox *bit_shift(new QSpinBox);
  bit_shift->setRange(0, 16);
  bit_shift->setValue(d.bit_shift);
  table->setCellWidget(table->rowCount() - 1, 3, bit_shift);
}

void ModbusTCPHandler::addInt16(const industrial_modbus_tcp::Int16 &d)
{
  QTableWidget *table(tables_.at(INT16));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);
}

void ModbusTCPHandler::addUInt16(const industrial_modbus_tcp::UInt16 &d)
{
  QTableWidget *table(tables_.at(UINT16));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);
}

void ModbusTCPHandler::addInt32(const industrial_modbus_tcp::Int32 &d)
{
  QTableWidget *table(tables_.at(INT32));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);
  addBigEndian(d.big_endian, table);
}

void ModbusTCPHandler::addUInt32(const industrial_modbus_tcp::UInt32 &d)
{
  QTableWidget *table(tables_.at(UINT32));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);
  addBigEndian(d.big_endian, table);
}

void ModbusTCPHandler::addInt64(const industrial_modbus_tcp::Int64 &d)
{
  QTableWidget *table(tables_.at(INT64));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);
  addBigEndian(d.big_endian, table);
}

void ModbusTCPHandler::addUInt64(const industrial_modbus_tcp::UInt64 &d)
{
  QTableWidget *table(tables_.at(UINT64));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);
  addBigEndian(d.big_endian, table);
}

void ModbusTCPHandler::addFloat32(const industrial_modbus_tcp::Float32 &d)
{
  QTableWidget *table(tables_.at(FLOAT32));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);
  addBigEndian(d.big_endian, table);
}

void ModbusTCPHandler::addFloat64(const industrial_modbus_tcp::Float64 &d)
{
  QTableWidget *table(tables_.at(FLOAT64));
  table->insertRow(table->rowCount());
  addRegister(d.reg, table);
  addBigEndian(d.big_endian, table);
}

void ModbusTCPHandler::addData()
{
  switch (tabs_->currentIndex())
  {
    case BOOL:
    {
      industrial_modbus_tcp::Bool d;
      d.reg.poll_rate_usec = 200000;
      addBool(d);
      break;
    }
    case INT8:
    {
      industrial_modbus_tcp::Int8 d;
      d.reg.poll_rate_usec = 200000;
      addInt8(d);
      break;
    }
    case UINT8:
    {
      industrial_modbus_tcp::UInt8 d;
      d.reg.poll_rate_usec = 200000;
      addUInt8(d);
      break;
    }
    case INT16:
    {
      industrial_modbus_tcp::Int16 d;
      d.reg.poll_rate_usec = 200000;
      addInt16(d);
      break;
    }
    case UINT16:
    {
      industrial_modbus_tcp::UInt16 d;
      d.reg.poll_rate_usec = 200000;
      addUInt16(d);
      break;
    }
    case INT32:
    {
      industrial_modbus_tcp::Int32 d;
      d.reg.poll_rate_usec = 200000;
      addInt32(d);
      break;
    }
    case UINT32:
    {
      industrial_modbus_tcp::UInt32 d;
      d.reg.poll_rate_usec = 200000;
      addUInt32(d);
      break;
    }
    case INT64:
    {
      industrial_modbus_tcp::Int64 d;
      d.reg.poll_rate_usec = 200000;
      addInt64(d);
      break;
    }
    case UINT64:
    {
      industrial_modbus_tcp::UInt64 d;
      d.reg.poll_rate_usec = 200000;
      addUInt64(d);
      break;
    }
    case FLOAT32:
    {
      industrial_modbus_tcp::Float32 d;
      d.reg.poll_rate_usec = 200000;
      addFloat32(d);
      break;
    }
    case FLOAT64:
    {
      industrial_modbus_tcp::Float64 d;
      addFloat64(d);
      break;
    }
    default:
      break;
  }
}

void ModbusTCPHandler::removeData()
{
  QTableWidget *table(tables_.at(tabs_->currentIndex()));
  table->removeRow(table->rowCount() - 1);
}

}
