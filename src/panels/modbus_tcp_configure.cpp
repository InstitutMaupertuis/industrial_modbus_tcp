#include <industrial_modbus_tcp/panels/modbus_tcp_configure.hpp>

namespace industrial_modbus_tcp
{

ModbusTCPConfigure::ModbusTCPConfigure(const industrial_modbus_tcp::ConfigureRequest &req)
{
  qRegisterMetaType<QMessageBox::Icon>();
  connect(this, &ModbusTCPConfigure::displayMessageBox, this, &ModbusTCPConfigure::displayMessageBoxHandler);

  setObjectName("Modbus TCP settings");
  setWindowTitle(objectName());
  resize(930, 570);

  layout_ = new QVBoxLayout();
  setLayout(layout_);

  QDialogButtonBox *buttons_box = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
  connect(buttons_box, &QDialogButtonBox::accepted, this, &ModbusTCPConfigure::checkHandlers);
  connect(buttons_box, &QDialogButtonBox::rejected, this, &ModbusTCPConfigure::reject);

  remove_handler_ = new QPushButton("Remove handler");
  QPushButton *add_handler = new QPushButton("Add handler");
  connect(remove_handler_, &QPushButton::clicked, this, &ModbusTCPConfigure::removeHandler);
  connect(add_handler, &QPushButton::clicked, this, qOverload<>(&ModbusTCPConfigure::addHandler));

  QHBoxLayout *handler_op_layout = new QHBoxLayout;
  handler_op_layout->addWidget(remove_handler_);
  handler_op_layout->addWidget(add_handler);

  tabs_ = new QTabWidget;

  for (auto &handler : req.handlers)
    addHandler(handler);

  layout_->addLayout(handler_op_layout);
  layout_->addWidget(tabs_);
  layout_->addWidget(buttons_box);

  if (tabs_->count() == 0)
    remove_handler_->setEnabled(false);
}

ModbusTCPConfigure::~ModbusTCPConfigure()
{
}

void ModbusTCPConfigure::getConfigureRequest(industrial_modbus_tcp::ConfigureRequest &req)
{
  // Verify handlers
  // Does not check for duplicate registers/topic names etc.
  // Does not check for dupplicate handler names
  req.handlers.clear();
  for (std::size_t i(0); i < (unsigned)tabs_->count(); ++i)
    req.handlers.push_back(static_cast<ModbusTCPHandler *>(tabs_->widget(i))->getHandler()); // ModbusTCP catches
}

void ModbusTCPConfigure::removeLastHandler()
{
  if (tabs_->count() == 0)
    return;

  tabs_->removeTab(tabs_->count() - 1);

  if (tabs_->count() == 0)
    remove_handler_->setEnabled(false);
}

void ModbusTCPConfigure::addHandler(const industrial_modbus_tcp::ModbusHandler h)
{
  ModbusTCPHandler *tab = new ModbusTCPHandler(h);
  connect(tab, &ModbusTCPHandler::nameChanged, this, &ModbusTCPConfigure::updateTabsNames);
  remove_handler_->setEnabled(true);
  tabs_->addTab(tab, "Handler");
  updateTabsNames();
}

void ModbusTCPConfigure::addHandler()
{
  industrial_modbus_tcp::ModbusHandler h;
  h.name = "Handler";
  h.ip_address = "192.168.100.000";
  h.port = 502;
  h.modbus_error_recovery_mode = 3;
  addHandler(h);
}

void ModbusTCPConfigure::removeHandler()
{
  tabs_->removeTab(tabs_->currentIndex());
}

void ModbusTCPConfigure::checkHandlers()
{
  try
  {
    for (std::size_t i(0); i < (unsigned)tabs_->count(); ++i)
      static_cast<ModbusTCPHandler *>(tabs_->widget(i))->getHandler();
  }
  catch (std::runtime_error &e)
  {
    Q_EMIT displayMessageBox("Error in handlers",
                             e.what(), "",
                             QMessageBox::Icon::Warning);
    return;
  }

  accept();
}

void ModbusTCPConfigure::updateTabsNames()
{
  for (std::size_t i(0); i < std::size_t(tabs_->count()); ++i)
    tabs_->setTabText(i, static_cast<ModbusTCPHandler *>(tabs_->widget(i))->name());
}

void ModbusTCPConfigure::displayMessageBoxHandler(const QString title,
    const QString text,
    const QString info,
    const QMessageBox::Icon icon)
{
  const bool old_state(isEnabled());
  setEnabled(false);
  QMessageBox msg_box;
  msg_box.setWindowTitle(title);
  msg_box.setText(text);
  msg_box.setInformativeText(info);
  msg_box.setIcon(icon);
  msg_box.setStandardButtons(QMessageBox::Ok);
  msg_box.exec();
  setEnabled(old_state);
}

}
