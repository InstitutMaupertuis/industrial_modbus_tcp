#ifndef IM_AM_MONITORING_QT_GUIS_MODBUS_TCP_CONFIGURE_HPP
#define IM_AM_MONITORING_QT_GUIS_MODBUS_TCP_CONFIGURE_HPP

#ifndef Q_MOC_RUN
#include <industrial_modbus_tcp/ConfigureRequest.h>
#include <industrial_modbus_tcp/panels/modbus_tcp_handler.hpp>
#include <ros/ros.h>
#endif

#include <QDialog>
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QPushButton>
#include <QTabWidget>
#include <QVBoxLayout>

namespace industrial_modbus_tcp
{

class ModbusTCPConfigure : public QDialog
{
  Q_OBJECT

public:
  ModbusTCPConfigure(const industrial_modbus_tcp::ConfigureRequest &req);
  virtual ~ModbusTCPConfigure();

  void getConfigureRequest(industrial_modbus_tcp::ConfigureRequest &req);

Q_SIGNALS:
  void displayMessageBox(const QString,
                         const QString,
                         const QString,
                         const QMessageBox::Icon);

private Q_SLOTS:
  void checkHandlers();
  void updateTabsNames();
  void addHandler();
  void removeHandler();
  void addHandler(const industrial_modbus_tcp::ModbusHandler h);
  void removeLastHandler();
  void displayMessageBoxHandler(const QString title,
                                const QString text,
                                const QString info = "",
                                const QMessageBox::Icon icon = QMessageBox::Icon::Information);
private:
  QVBoxLayout *layout_;
  QTabWidget *tabs_;
  QPushButton *remove_handler_;
};

}

#endif
