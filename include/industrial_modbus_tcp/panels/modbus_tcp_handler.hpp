#ifndef IM_AM_MONITORING_QT_GUIS_MODBUS_TCP_HANDLER_HPP
#define IM_AM_MONITORING_QT_GUIS_MODBUS_TCP_HANDLER_HPP

#ifndef Q_MOC_RUN
#include <industrial_modbus_tcp/ConfigureRequest.h>
#include <ros/ros.h>
#endif

#include <QComboBox>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QSpinBox>
#include <QTableWidget>
#include <QTabWidget>
#include <QVBoxLayout>

namespace industrial_modbus_tcp
{

class ModbusTCPHandler : public QWidget
{
  enum TAB_INDEX : unsigned { BOOL, INT8, UINT8, INT16, UINT16, INT32, UINT32, INT64, UINT64, FLOAT32, FLOAT64, SIZE};

  Q_OBJECT

public:
  ModbusTCPHandler(industrial_modbus_tcp::ModbusHandler h = industrial_modbus_tcp::ModbusHandler());
  virtual ~ModbusTCPHandler();

  industrial_modbus_tcp::ModbusHandler getHandler();
  QString name();

Q_SIGNALS:
  void nameChanged();

public Q_SLOTS:
  void addRegister(const industrial_modbus_tcp::Register &r, QTableWidget *table);
  void addBigEndian(const bool value, QTableWidget *table);
  void addBool(const industrial_modbus_tcp::Bool &d);
  void addInt8(const industrial_modbus_tcp::Int8 &d);
  void addUInt8(const industrial_modbus_tcp::UInt8 &d);
  void addInt16(const industrial_modbus_tcp::Int16 &d);
  void addUInt16(const industrial_modbus_tcp::UInt16 &d);
  void addInt32(const industrial_modbus_tcp::Int32 &d);
  void addUInt32(const industrial_modbus_tcp::UInt32 &d);
  void addInt64(const industrial_modbus_tcp::Int64 &d);
  void addUInt64(const industrial_modbus_tcp::UInt64 &d);
  void addFloat32(const industrial_modbus_tcp::Float32 &d);
  void addFloat64(const industrial_modbus_tcp::Float64 &d);

private Q_SLOTS:
  void addData();
  void removeData();

private:
  void readRegisterTable(const QTableWidget *table,
                         const unsigned row,
                         industrial_modbus_tcp::Register &r);

  QVBoxLayout *layout_;
  QLineEdit *name_;
  QLineEdit *ip_address_;
  QSpinBox *port_;
  QComboBox *recovery_;
  QTabWidget *tabs_;
  std::vector<QTableWidget *> tables_;
  QPushButton *add_data_;
  QPushButton *remove_data_;
};

}

#endif
