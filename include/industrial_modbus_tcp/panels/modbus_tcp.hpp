#ifndef IM_AM_MONITORING_QT_GUIS_MODBUS_TCP_HPP
#define IM_AM_MONITORING_QT_GUIS_MODBUS_TCP_HPP

#include <industrial_modbus_tcp/Configure.h>
#include <industrial_modbus_tcp/panels/modbus_tcp_configure.hpp>
#include <QDateTime>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QStackedWidget>
#include <QtConcurrent/QtConcurrentRun>
#include <QVBoxLayout>
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#include <std_msgs/String.h>

namespace industrial_modbus_tcp
{

class ModbusTCP : public rviz::Panel
{
  Q_OBJECT

public:
  ModbusTCP(QWidget *parent = NULL);
  virtual ~ModbusTCP();

Q_SIGNALS:
  void enable(const bool);
  void newError(const QString, const QString);
  void displayMessageBox(const QString,
                         const QString,
                         const QString,
                         const QMessageBox::Icon);
  void setStackIndex(const int);

protected Q_SLOTS:
  void configureButtonHandler();
  void newErrorHandler(const QString, const QString);
  void configure();
  virtual void load(const rviz::Config &config);
  bool parseDataReg(const rviz::Config &config,
                    const unsigned i,
                    const unsigned j,
                    const std::string name,
                    industrial_modbus_tcp::Register &r);
  virtual void saveDataReg(rviz::Config &config,
                           const unsigned i,
                           const unsigned j,
                           const std::string name,
                           const industrial_modbus_tcp::Register &r) const;
  virtual void save(rviz::Config config) const;
  void displayMessageBoxHandler(const QString title,
                                const QString text,
                                const QString info,
                                const QMessageBox::Icon icon);

private:
  void connectToService();
  void errorsCallback(const std_msgs::String::ConstPtr &msg);

  QTableWidget *errors_log_;
  QStackedWidget *stack_;
  QLineEdit *base_topic_line_edit_;
  QPushButton *change_base_topic_;

  std::string base_topic_;
  ros::NodeHandle nh_;
  ros::Subscriber errors_sub_;
  ros::ServiceClient configure_modbus_;
  industrial_modbus_tcp::Configure configure_modbus_srv_;
};

}

#endif
