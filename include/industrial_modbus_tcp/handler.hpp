#ifndef INDUSTRIAL_MODBUS_TCP_HANDLER_HPP
#define INDUSTRIAL_MODBUS_TCP_HANDLER_HPP

#include <atomic>
#include <industrial_modbus_tcp/data_type.hpp>
#include <modbus/modbus.h>
#include <mutex>
#include <std_msgs/String.h>
#include <thread>

namespace industrial_modbus_tcp
{

uint32_t freq_to_usecs(const double freq)
{
  if (!(freq > 0.0))
    throw std::runtime_error("Frequency must be > 0");

  return (1 / freq) * 1e6;
}

class Handler
{
public:
  Handler(const std::shared_ptr<ros::NodeHandle> nh,
          const std::string name,
          const std::shared_ptr<ros::Publisher> error_pub,
          const std::string server_ip,
          const modbus_error_recovery_mode recovery = MODBUS_ERROR_RECOVERY_NONE,
          const unsigned server_port = MODBUS_TCP_DEFAULT_PORT);

  ~Handler();

  /**
   * Stop fetching data
   */
  void stop();

  /**
   * Clear vector of data fetched (calls stop())
   */
  void clearData();

  /**
   * Set data to be fetched (starts fetching data)
   */
  void setData(const std::vector<std::shared_ptr<DataType>> &data);

  /**
   * Data that are currently fetched
   */
  const std::vector<std::shared_ptr<DataType>> getData();

private:
  /**
   * Node handle
   */
  std::shared_ptr<ros::NodeHandle> nh_;

  /**
   * Publisher for errors
   */
  std::shared_ptr<ros::Publisher> error_publisher_;

  /**
   * Thread safe function to retrieve data using the information in data_
   * @param index the data_ index
   */
  void readData(const std::size_t index);

  /**
   * Wether the threads are running or not
   */
  std::atomic<bool> run_;

  /**
   * Name of the handler
   */
  std::string name_;

  /**
   * Modbus structure
   */
  modbus_t *ctx_;

  /**
   * Data to be fetched on the Modbus server
   * How fast
   * Topic to publish
   */
  std::vector<std::shared_ptr<DataType>> data_;

  /**
   * A modbus context is supposedely thread safe (https://libmodbus.org/docs/v3.0.6/)
   * This is not the case, so this mutex makes sure we have no concurent calls to the modbus conctext
   */
  std::mutex modbus_mutex_;

  /**
   * One thread per data fetched
   */
  std::vector<std::unique_ptr<std::thread>> threads_;
};

}

#endif
