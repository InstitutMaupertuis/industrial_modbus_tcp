#ifndef INDUSTRIAL_MODBUS_TCP_DATA_TYPE_HPP
#define INDUSTRIAL_MODBUS_TCP_DATA_TYPE_HPP

#include <array>
#include <chrono>
#include <memory>
#include <bitset>
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/UInt64.h>
#include <std_msgs/UInt8.h>
#include <string>

namespace industrial_modbus_tcp
{

class DataType
{
public:
  DataType(const std::shared_ptr<ros::NodeHandle> &nh,
           const std::size_t number_of_registers,
           const uint64_t start_register_address,
           const std::chrono::microseconds poll_rate,
           const std::string topic_name):
    number_of_registers_(number_of_registers),
    start_register_address_(start_register_address),
    poll_rate_(poll_rate),
    topic_name_(topic_name),
    nh_(nh)
  {
  }

  /**
   * Checks if regs has the correct size, converts and publishes data
   */
  virtual void convertAndPublishData(const std::vector<uint16_t> &regs) = 0;

  const std::size_t number_of_registers_;
  const uint64_t start_register_address_;
  const std::chrono::microseconds poll_rate_;
  const std::string topic_name_;

protected:
  /**
   * Check if regs has the correct size compared to number_of_registers_
   */
  void checkRegistersSize(const std::vector<uint16_t> &regs)
  {
    if (regs.size() != number_of_registers_)
      throw std::runtime_error(topic_name_ + ": registers do not have the expected size (" +
                               std::to_string(regs.size()) + " vs " + std::to_string(number_of_registers_) + ")");
  }

  ros::Publisher pub_;
  std::shared_ptr<ros::NodeHandle> nh_;
  bool first_ = true;
};

class BoolType: public DataType
{
public:
  BoolType(const std::shared_ptr<ros::NodeHandle> &nh,
           const uint64_t start_register_address,
           const std::chrono::microseconds poll_rate,
           const std::string topic_name):
    DataType(nh, 1, start_register_address, poll_rate, topic_name)
  {
    pub_ = nh_->advertise<std_msgs::Bool>(topic_name, 5, true);
  }

  uint16_t mask_ = 0x0;

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std::bitset<16> bits(regs.at(0));
    std_msgs::Bool value;
    if(bits[mask_])
      value.data = 1;
    else
      value.data = 0;
    if (first_ || value.data != last_.data)
    {
      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::Bool last_;
};

class Int8Type: public DataType
{
public:
  Int8Type(const std::shared_ptr<ros::NodeHandle> &nh,
           const uint64_t start_register_address,
           const std::chrono::microseconds poll_rate,
           const std::string topic_name):
    DataType(nh, 1, start_register_address, poll_rate, topic_name)
  {
    pub_ = nh_->advertise<std_msgs::Int8>(topic_name, 5, true);
  }

  uint8_t bit_shift = 0;

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::Int8 value;
    value.data = regs.at(0) >> bit_shift;
    if (first_ || value.data != last_.data)
    {
      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::Int8 last_;
};

class UInt8Type: public DataType
{
public:
  UInt8Type(const std::shared_ptr<ros::NodeHandle> &nh,
            const uint64_t start_register_address,
            const std::chrono::microseconds poll_rate,
            const std::string topic_name):
    DataType(nh, 1, start_register_address, poll_rate, topic_name)
  {
    pub_ = nh_->advertise<std_msgs::UInt8>(topic_name, 5, true);
  }

  uint8_t bit_shift = 0;

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::UInt8 value;
    value.data = static_cast<uint8_t>(regs.at(0) >> bit_shift);
    if (first_ || value.data != last_.data)
    {
      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::UInt8 last_;
};

class Int16Type: public DataType
{
public:
  Int16Type(const std::shared_ptr<ros::NodeHandle> &nh,
            const uint64_t start_register_address,
            const std::chrono::microseconds poll_rate,
            const std::string topic_name):
    DataType(nh, 1, start_register_address, poll_rate, topic_name)
  {
    pub_ = nh_->advertise<std_msgs::Int16>(topic_name, 5, true);
  }

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::Int16 value;
    value.data = regs.at(0);
    if (first_ || value.data != last_.data)
    {
      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::Int16 last_;
};

class UInt16Type: public DataType
{
public:
  UInt16Type(const std::shared_ptr<ros::NodeHandle> &nh,
             const uint64_t start_register_address,
             const std::chrono::microseconds poll_rate,
             const std::string topic_name):
    DataType(nh, 1, start_register_address, poll_rate, topic_name)
  {
    pub_ = nh_->advertise<std_msgs::UInt16>(topic_name, 5, true);
  }

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::UInt16 value;
    value.data = static_cast<uint16_t>(regs.at(0));
    if (first_ || value.data != last_.data)
    {
      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::UInt16 last_;
};

class Int32Type: public DataType
{
public:
  Int32Type(const std::shared_ptr<ros::NodeHandle> &nh,
            const uint64_t start_register_address,
            const std::chrono::microseconds poll_rate,
            const std::string topic_name):
    DataType(nh, 2, start_register_address, poll_rate, topic_name)
  {
    pub_ = nh_->advertise<std_msgs::Int32>(topic_name, 5, true);
  }

  bool big_endian = false;

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::Int32 value;

    if (!big_endian)
      value.data = regs.at(0) + (((int32_t) regs.at(1)) << 16);
    else
      value.data = regs.at(1) + (((int32_t) regs.at(0)) << 16);

    if (first_ || value.data != last_.data)
    {
      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::Int32 last_;
};

class UInt32Type: public DataType
{
public:
  UInt32Type(const std::shared_ptr<ros::NodeHandle> &nh,
             const uint64_t start_register_address,
             const std::chrono::microseconds poll_rate,
             const std::string topic_name):
    DataType(nh, 2, start_register_address, poll_rate, topic_name)
  {
    pub_ = nh_->advertise<std_msgs::UInt32>(topic_name, 5, true);
  }

  bool big_endian = false;

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::UInt32 value;

    if (!big_endian)
      value.data = regs.at(0) + (((uint32_t) regs.at(1)) << 16);
    else
      value.data = regs.at(1) + (((uint32_t) regs.at(0)) << 16);

    if (first_ || value.data != last_.data)
    {
      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::UInt32 last_;
};

class Int64Type: public DataType
{
public:
  Int64Type(const std::shared_ptr<ros::NodeHandle> &nh,
            const uint64_t start_register_address,
            const std::chrono::microseconds poll_rate,
            const std::string topic_name):
    DataType(nh, 4, start_register_address, poll_rate, topic_name)
  {
    pub_ = nh_->advertise<std_msgs::Int64>(topic_name, 5, true);
  }

  bool big_endian = false;

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::Int64 value;

    if (!big_endian)
    {
      value.data = regs.at(0);
      value.data += (((int64_t) regs.at(1)) << 16);
      value.data += (((int64_t) regs.at(2)) << 32);
      value.data += (((int64_t) regs.at(3)) << 48);
    }
    else
    {
      value.data = regs.at(3);
      value.data += (((int64_t) regs.at(2)) << 16);
      value.data += (((int64_t) regs.at(1)) << 32);
      value.data += (((int64_t) regs.at(0)) << 48);
    }

    if (first_ || value.data != last_.data)
    {
      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::Int64 last_;
};

class UInt64Type: public DataType
{
public:
  UInt64Type(const std::shared_ptr<ros::NodeHandle> &nh,
            const uint64_t start_register_address,
            const std::chrono::microseconds poll_rate,
            const std::string topic_name):
    DataType(nh, 4, start_register_address, poll_rate, topic_name)
  {
    pub_ = nh_->advertise<std_msgs::UInt64>(topic_name, 5, true);
  }

  bool big_endian = false;

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::UInt64 value;

    if (!big_endian)
    {
      value.data = regs.at(0);
      value.data += (((uint64_t) regs.at(1)) << 16);
      value.data += (((uint64_t) regs.at(2)) << 32);
      value.data += (((uint64_t) regs.at(3)) << 48);
    }
    else
    {
      value.data = regs.at(3);
      value.data += (((uint64_t) regs.at(2)) << 16);
      value.data += (((uint64_t) regs.at(1)) << 32);
      value.data += (((uint64_t) regs.at(0)) << 48);
    }

    if (first_ || value.data != last_.data)
    {
      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::UInt64 last_;
};

class Float32Type: public DataType
{
public:
  Float32Type(const std::shared_ptr<ros::NodeHandle> &nh,
              const uint64_t start_register_address,
              const std::chrono::microseconds poll_rate,
              const std::string topic_name):
    DataType(nh, 4, start_register_address, poll_rate, topic_name)
  {
    assert(sizeof(float) == sizeof(uint32_t));
    pub_ = nh_->advertise<std_msgs::Float32>(topic_name, 5, true);
  }

  bool big_endian = false;

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::Float32 value;

    uint32_t uint_float(0);
    if (!big_endian)
      uint_float = regs.at(0) + (((uint32_t) regs.at(1)) << 16);
    else
      uint_float = regs.at(1) + (((uint32_t) regs.at(0)) << 16);
    value.data = *(reinterpret_cast<float *>(&uint_float));

    if (first_ || value.data != last_.data)
    {
      if (std::isnan(last_.data) && std::isnan(value.data))
        return;

      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::Float32 last_;
};

class Float64Type: public DataType
{
public:
  Float64Type(const std::shared_ptr<ros::NodeHandle> &nh,
              const uint64_t start_register_address,
              const std::chrono::microseconds poll_rate,
              const std::string topic_name):
    DataType(nh, 4, start_register_address, poll_rate, topic_name)
  {
    assert(sizeof(double) == sizeof(uint64_t));
    pub_ = nh_->advertise<std_msgs::Float64>(topic_name, 5, true);
  }

  bool big_endian = false;

  void convertAndPublishData(const std::vector<uint16_t> &regs)
  {
    checkRegistersSize(regs);
    std_msgs::Float64 value;

    uint64_t uint_double(0);
    if (!big_endian)
    {
      uint_double = regs.at(0);
      uint_double += ((uint64_t) regs.at(1) << 16);
      uint_double += ((uint64_t) regs.at(2) << 32);
      uint_double += ((uint64_t) regs.at(3) << 48);
    }
    else
    {
      uint_double = regs.at(3);
      uint_double += ((uint64_t) regs.at(2) << 16);
      uint_double += ((uint64_t) regs.at(1) << 32);
      uint_double += ((uint64_t) regs.at(0) << 48);
    }
    value.data = *(reinterpret_cast<double *>(&uint_double));

    if (first_ || value.data != last_.data)
    {
      if (std::isnan(last_.data) && std::isnan(value.data))
        return;

      pub_.publish(value);
      last_ = value;
      first_ = false;
    }
  }

private:
  std_msgs::Float64 last_;
};

}

#endif
